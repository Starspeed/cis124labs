var code;
var question;
var msg;

//Global variables
//create variables for all DOM objects that will be part of the
//application interactions

// Document ready:
$(function() {
    $("#magic8ball").click(function(){
        reset8Ball();
    });
    

    $("form").submit(function(e){
        e.preventDefault();
        if($("#question").val() != ""){
            getFortune();
        }
        else{
            showAnswer("My chakras are blocked");
        }
        /*
            Ideally, you would want to get a fortune only if a
            question was actually provided in the text input.
            If the question input is empty, have an appropriate
            message sent to showAnswer to reflect that.
        */
    });

});

//Helper functions
var getRandomIndex = function(max) {
    code = Math.random();;
    /*
        Since $('#question').val() produces a string
        it will always result in a value of zero
        once it is processed in the PHP script.
        
        Make sure that "code" ends up with a random
        integer value instead. You can use a simple
        application of the Math.random() function to
        do this, but make sure you end up with some
        integer value being stored in "code"
    */
};

var showAnswer = function(msg) {

    $("img").fadeOut('fast');
    $("#answer").html(msg).fadeIn('fast');

};

var getFortune = function() {
    getRandomIndex();
    $.ajax({        
        url: 'php/fortune.php',
        type: "post",
        data: {"code":code},
        success: function(response){showAnswer(response);},
        error:function(xhr){showAnswer("My chakras are totally blocked man")}
        
    });
};


var reset8Ball = function() {
    $("#magic8ball").effect("bounce", {
        times: 2}, "slow")
    $("#answer").html(msg).fadeOut('fast');
    $("img").fadeIn('fast'); 
};