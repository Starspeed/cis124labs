<?php

$fortunes = array("It is certain","It is decidedly so","Without a doubt","Yes, definitely","You may rely on it","As I see it, yes","Most likely","Outlook good","Yes","Signs point to yes","Reply hazy try again","Ask again later","Better not tell you now","Cannot predict now","Concentrate and ask again","Dont count on it","My reply is no","My sources say no","Outlook not so good","Very doubtful");

shuffle($fortunes);
$input = intval($_POST["code"]) % count($fortunes);
//Use a value passed into this script to calculate
//a random integer that is a valid index in the
//fortunes array

echo($fortunes[$input]);
//Use the 'echo' command to print the fortune
//located at the random index value in the array

?>